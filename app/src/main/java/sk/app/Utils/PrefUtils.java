package sk.app.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by vander on 6/4/14.
 */
public class PrefUtils {
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String PREF_LOCAL_DATA_VERSION = "local_data_version";
    private static final String PREF_LOGIN = "user_login";

    private static SharedPreferences getPrefs(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    private static SharedPreferences.Editor editPrefs(Context ctx) {
        return getPrefs(ctx).edit();
    }

    public static boolean hasUserLearnedDrawer(Context ctx) {
        return getPrefs(ctx).getBoolean(PREF_USER_LEARNED_DRAWER, false);
    }

    public static void setUserLearnedDrawer(Context ctx) {
        editPrefs(ctx)
                .putBoolean(PREF_USER_LEARNED_DRAWER, true)
                .apply();
    }

    public static int getLocalDataVersion(Context ctx) {
        return getPrefs(ctx).getInt(PREF_LOCAL_DATA_VERSION, 0);
    }

    public static void setLocalDataVersion(Context ctx, int version) {
        editPrefs(ctx)
                .putInt(PREF_LOCAL_DATA_VERSION, version)
                .apply();
    }

    public static void setUserLogin(Context context, String login) {
        editPrefs(context).putString(PREF_LOGIN, login).apply();
    }

    public static String getUserLogin(Context context) {
        return getPrefs(context).getString(PREF_LOGIN, "");
    }

    public static void clear(Context context) {
        editPrefs(context).clear().commit();
    }
}
