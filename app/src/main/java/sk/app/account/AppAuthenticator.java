package sk.app.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.facebook.LoginActivity;

import sk.app.R;
import sk.app.Utils.LogUtils;
import sk.app.ui.AuthenticatorActivity;

public class AppAuthenticator extends AbstractAccountAuthenticator {
    private static final int ERROR_CODE_ONE_ACCOUNT_ALLOWED = 40000;
    private static final String TAG = LogUtils.makeLogTag(AppAuthenticator.class);
    private final Context mContext;
    private final Handler mHandler;

    public AppAuthenticator(Context context) {
        super(context);
        this.mContext = context;
        mHandler = new Handler();
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType,
                             String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        final AccountManager accountManager = AccountManager.get(mContext);
        final Account[] accounts = accountManager.getAccountsByType(accountType);
        if (accounts.length > 0) {
            final Bundle bundle = new Bundle();
            bundle.putInt(AccountManager.KEY_ERROR_CODE, ERROR_CODE_ONE_ACCOUNT_ALLOWED);
            bundle.putString(AccountManager.KEY_ERROR_MESSAGE, mContext.getString(R.string.one_account_allowed));

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext.getApplicationContext(), R.string.one_account_allowed,
                            Toast.LENGTH_LONG).show();
                }
            });
            return bundle;
        }

        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle
            options) throws NetworkErrorException {
        if (AuthUtils.AUTH_TOKEN_TYPE.equals(authTokenType)) {
            final Intent intent = new Intent(mContext, LoginActivity.class);
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
            intent.putExtra(AuthenticatorActivity.EXTRA_EMAIL, account.name);
            final Bundle bundle = new Bundle();
            bundle.putParcelable(AccountManager.KEY_INTENT, intent);
            return bundle;
        }

        final Bundle errorResult = new Bundle();
        errorResult.putInt(AccountManager.KEY_ERROR_CODE, AccountManager.ERROR_CODE_INVALID_RESPONSE);
        errorResult.putString(AccountManager.KEY_ERROR_MESSAGE, "authtoken type");
        return errorResult;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType,
                                    Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        final Bundle bundle = new Bundle();
        bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return bundle;
    }
}
