package sk.app.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;

import sk.app.Utils.PrefUtils;
import sk.app.provider.AppContract;
import sk.app.ui.AuthenticatorActivity;

import static sk.app.Utils.LogUtils.LOGD;
import static sk.app.Utils.LogUtils.makeLogTag;

public final class AuthUtils {

    private static final String TAG = makeLogTag(AuthUtils.class);
    public static final String ACCOUNT_TYPE = "sk.app.account";
    public static final String AUTH_TOKEN_TYPE = "app_auth_token";
    public static final String USER_DATA_RTOKEN = "refresh_token";

    private AuthUtils() {}

    public static boolean isUserAuthenticated(Context context) {
        final String userId = PrefUtils.getUserLogin(context);

        if (!TextUtils.isEmpty(userId)) {
            final AccountManager accountManager = AccountManager.get(context);
            final Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);

            if (accounts.length == 1 && userId.equals(accounts[0].name)) {
                return true;
            } else {
                for (Account account : accounts) {
                    accountManager.removeAccount(account, null, null);
                }
            }
        }

        return false;
    }

    public static void startAuthenticationFlow(Context context, Intent finishIntent) {
        final Intent loginFlowIntent = new Intent(context, AuthenticatorActivity.class);
        loginFlowIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginFlowIntent.putExtra(AuthenticatorActivity.EXTRA_FINISH_INTENT, finishIntent);
        context.startActivity(loginFlowIntent);
    }

    public static void signOut(Context context) {
        PrefUtils.clear(context);

        final AccountManager am = AccountManager.get(context);
        final Account account = getAccount(am);
        if (account != null) {
            am.removeAccount(account, null, null);
        }
        context.getContentResolver().delete(AppContract.BASE_CONTENT_URI, null, null);
    }

    public static boolean createAccountHTTPBasic(Context context, String login, String password) {
        final AccountManager accountManager = AccountManager.get(context);
        final Account account = new Account(login, AuthUtils.ACCOUNT_TYPE);

        final boolean success = accountManager.addAccountExplicitly(account,
                getBasicHttpString(login, password), null);

        if (success) {
            PrefUtils.setUserLogin(context, login);
//            ContentResolver.setSyncAutomatically(account, AppContract.CONTENT_AUTHORITY, true);

            return true;
        }

        return false;
    }

    private static String getBasicHttpString(String login, String password) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Basic ");
        stringBuilder.append(Base64.encodeToString((login + ":" + password).getBytes(), Base64.DEFAULT));

        return stringBuilder.toString();
    }

    public static boolean createAccount(Context context, String login, String password, String accessToken,
                                        String refreshToken) {
        final AccountManager accountManager = AccountManager.get(context);
        final Account account = new Account(login, AuthUtils.ACCOUNT_TYPE);

        final Bundle userData = new Bundle();
        userData.putString(USER_DATA_RTOKEN, refreshToken);
        final boolean success = accountManager.addAccountExplicitly(account,
                password, userData);

        if (success) {
            accountManager.setAuthToken(account, AUTH_TOKEN_TYPE, accessToken);
            PrefUtils.setUserLogin(context, login);
//            ContentResolver.setSyncAutomatically(account, AppContract.CONTENT_AUTHORITY, true);
            return true;
        }
        return false;
    }

    public static boolean updateAccount(Context context, String login, String authToken,
                                        String refreshToken, String password) {
        final AccountManager accountManager = AccountManager.get(context);
        final Account account = getAccount(accountManager);

        if (account != null && account.name.equals(login)) {
            accountManager.setPassword(account, password);
            accountManager.setAuthToken(account, AUTH_TOKEN_TYPE, authToken);
            accountManager.setUserData(account, USER_DATA_RTOKEN, refreshToken);
            return true;
        }
        return false;
    }

    public static Account getAccount(AccountManager am) {
        if (am != null) {
            final Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE);
            return accounts.length > 0 ? accounts[0] : null;
        }

        return null;
    }

    /**
     * @param context   context
     * @param showNotif whether to show notification which shows AuthActivity, if false AuthActivity is shown
     *                  immediately
     * @return token auth token
     */
    public static String getAuthToken(final Context context, boolean showNotif) {
        final AccountManager am = AccountManager.get(context);
        final Account account = getAccount(am);
        if (account == null) {
            return null;
        }

        final AccountManagerFuture<Bundle> future = getAccountManagerFuture(am, account, showNotif);

        final Bundle result;
        try {
            result = future.getResult();
            final String token = result.getString(AccountManager.KEY_AUTHTOKEN);
            return token;
        } catch (OperationCanceledException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        } catch (AuthenticatorException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        }

        return null;
    }

    public static String getAuthTokenFromActivity(final Context context, Activity activity) {
        final AccountManager am = AccountManager.get(context);
        final Account account = getAccount(am);
        if (account == null) {
            return null;
        }

        final AccountManagerFuture<Bundle> future = am.getAuthToken(account, AUTH_TOKEN_TYPE, null, activity,
                null, null);

        try {
            final Bundle result = future.getResult();
            final String token = result.getString(AccountManager.KEY_AUTHTOKEN);
            return token;
        } catch (OperationCanceledException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        } catch (AuthenticatorException e) {
            //TODO ohandlovat
            LOGD(TAG, Log.getStackTraceString(e));
        }

        return null;
    }


    private static AccountManagerFuture<Bundle> getAccountManagerFuture(AccountManager am, Account account, boolean showNotif) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return am.getAuthToken(account, AUTH_TOKEN_TYPE, null, showNotif, null,
                    null);
        } else {
            return am.getAuthToken(account, AUTH_TOKEN_TYPE, showNotif, null, null);
        }
    }

    private static String getAuthToken(AccountManager am) {
        final Account account = getAccount(am);
        if (account != null) {
            return am.peekAuthToken(getAccount(am), AUTH_TOKEN_TYPE);
        }

        return null;
    }

    private static void invalidateAuthToken(AccountManager am) {
        am.invalidateAuthToken(ACCOUNT_TYPE, getAuthToken(am));
    }

    public static void invalidate(Context context) {
        final AccountManager am = AccountManager.get(context);
        invalidateAuthToken(am);
    }
}
