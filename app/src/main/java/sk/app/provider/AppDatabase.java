/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sk.app.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static sk.app.Utils.LogUtils.LOGD;
import static sk.app.Utils.LogUtils.LOGI;
import static sk.app.Utils.LogUtils.LOGW;
import static sk.app.Utils.LogUtils.makeLogTag;

/**
 * Helper for managing {@link android.database.sqlite.SQLiteDatabase} that stores data for
 * {@link AppProvider}.
 */
public class AppDatabase extends SQLiteOpenHelper {
    private static final String TAG = makeLogTag(AppDatabase.class);

    private static final String DATABASE_NAME = "app.db";

    // NOTE: carefully update onUpgrade() when bumping database versions to make
    // sure user data is saved.

    private static final int VER_ALPHA = 11;  // 1.0
    private static final int VER_BETA = 2;  // 1.1
    private static final int DATABASE_VERSION = VER_ALPHA;

    private final Context mContext;

    interface Tables {
    }

    private interface Triggers {
    }

    /**
     * Fully-qualified field names.
     */
    private interface Qualified {
    }

    /**
     * {@code REFERENCES} clauses.
     */
    private interface References {
    }

    public AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
/*
        db.execSQL("CREATE TABLE " + Tables.BLOCKS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + BlocksColumns.BLOCK_ID + " TEXT NOT NULL,"
                + BlocksColumns.BLOCK_TITLE + " TEXT NOT NULL,"
                + BlocksColumns.BLOCK_START + " INTEGER NOT NULL,"
                + BlocksColumns.BLOCK_END + " INTEGER NOT NULL,"
                + BlocksColumns.BLOCK_TYPE + " TEXT,"
                + BlocksColumns.BLOCK_META + " TEXT,"
                + "UNIQUE (" + BlocksColumns.BLOCK_ID + ") ON CONFLICT REPLACE)");
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGD(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

        // Cancel any sync currently in progress
/*
        Account account = AccountUtils.getChosenAccount(mContext);
        if (account != null) {
            LOGI(TAG, "Cancelling any pending syncs for for account");
            ContentResolver.cancelSync(account, AppContract.CONTENT_AUTHORITY);
        }
*/

        // NOTE: This switch statement is designed to handle cascading database
        // updates, starting at the current version and falling through to all
        // future upgrade cases. Only use "break;" when you want to drop and
        // recreate the entire database.
        int version = oldVersion;

        switch (version) {
            // Note: Data from prior years not preserved.
            case VER_ALPHA:
                LOGI(TAG, "Performing migration for DB version " + version);
            case VER_BETA:
                version = VER_BETA;
                LOGI(TAG, "DB at version " + version);
                // Current version, no further action necessary
        }

        LOGD(TAG, "after upgrade logic, at version " + version);
        if (version != DATABASE_VERSION) {
            LOGW(TAG, "Destroying old data during upgrade");

//            db.execSQL("DROP TABLE IF EXISTS " + Tables.BLOCKS);
            onCreate(db);
        }

/*
        if (account != null) {
            LOGI(TAG, "DB upgrade complete. Requesting resync.");
            SyncHelper.requestManualSync(account);
        }
*/
    }

    public static void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }
}
