package sk.app.ui;

import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.IOException;
import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import sk.app.R;
import sk.app.Utils.NetUtils;
import sk.app.Utils.PrefUtils;
import sk.app.Utils.Utils;
import sk.app.account.AuthUtils;
import sk.app.ui.fragment.AlertDialogFragment;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class AuthenticatorActivity extends BaseAuthenticatorActivity
        implements TextView.OnEditorActionListener {

    public static final String EXTRA_EMAIL = "extra_email";
    public static final String EXTRA_REGISTER = "extra_register";
    public static final String EXTRA_FINISH_INTENT = "extra_finish_intent";

    public static final int RESULT_CODE_SUCCESS = 1000;
    public static final int RESULT_CODE_CANCELED = 1001;
    public static final int RESULT_CODE_FAILURE = 1002;

    private static final String POST_AUTH_CATEGORY
            = "sk.app.category.POST_AUTH";
    private static final String ERROR_DIALOG_TAG = "error_dialog";
    private static final int ACCOUNT_REQUEST = 4742;
    private static final int RESOLVE_GOOGLE_REQUEST = 9312;

    private Intent mFinishIntent;
    private boolean mIsRequestingAuthToken;
    private boolean mIsRegister;
    private String mAccountName;

    // UI references.
    @InjectView(R.id.email)
    protected EditText mEmailView;
    @InjectView(R.id.password)
    protected EditText mPasswordView;
    @InjectView(R.id.password_confirm)
    protected EditText mPasswordConfirmView;
    @InjectView(R.id.sign_in_button)
    protected Button mSignBtn;
    @InjectView(R.id.login_form)
    protected View mLoginFormView;
    @InjectView(R.id.login_status)
    protected View mLoginStatusView;
    @InjectView(R.id.login_status_message)
    protected TextView mLoginStatusMessageView;
    @InjectView(R.id.g_login)
    protected TextView mGLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_authenticate);
        ButterKnife.inject(this);

        final String mail = getIntent().getStringExtra(EXTRA_EMAIL);
        mFinishIntent = getIntent().getParcelableExtra(EXTRA_FINISH_INTENT);
        mIsRequestingAuthToken = !TextUtils.isEmpty(mail);
        mIsRegister = getIntent().getBooleanExtra(EXTRA_REGISTER, false);

        mEmailView.setText(mail);
        mEmailView.setEnabled(!mIsRequestingAuthToken);
        if (mIsRequestingAuthToken) {
            mPasswordView.requestFocus();
        }

        mPasswordConfirmView.setVisibility(mIsRegister ? View.VISIBLE : View.GONE);
        mSignBtn.setText(mIsRegister ? R.string.action_register : R.string.action_sign_in);
        setTitle(mIsRegister ? R.string.action_register : R.string.action_sign_in);

        if (mIsRegister) {
            mPasswordConfirmView.setOnEditorActionListener(this);
        } else {
            mPasswordView.setOnEditorActionListener(this);
            mPasswordView.setImeActionLabel(getString(R.string.action_sign_in), 10091);
            mPasswordView.setImeOptions(EditorInfo.IME_ACTION_UNSPECIFIED);
        }

        mGLogin.setVisibility(supportsGooglePlayServices() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mIsRequestingAuthToken) {
            finishSetup(RESULT_CODE_CANCELED, null);
        }
    }

    private boolean checkNet() {
        if (!NetUtils.isOnline(this)) {
            AlertDialogFragment.newInstance(getString(R.string.error_title), getString(R.string.error_no_internet))
                    .show(getFragmentManager(), ERROR_DIALOG_TAG);
            return false;
        }
        return true;
    }

    @OnClick(R.id.g_login)
    void googleLogin() {
        if (!checkNet()) {
            return;
        }

        showProgress(true);
        final Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"},
                false, null, null, null, null);
        startActivityForResult(intent, ACCOUNT_REQUEST);
    }

    @OnClick(R.id.fb_login)
    void facebookLogin() {
        if (!checkNet()) {
            return;
        }

        showProgress(true);
        final Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            session.closeAndClearTokenInformation();
        }
        Session.openActiveSession(this, true, Arrays.asList("email"), new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (state.isOpened()) {
                    fetchMailAndLogin(session);
                } else if (state == SessionState.CLOSED_LOGIN_FAILED) {
                    showProgress(false);
                }
            }
        });
    }

//    todo implement fb login
    private void fetchMailAndLogin(Session session) {
        final String token = session.getAccessToken();
        final Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (user != null) {
                            final String name = (String) user.asMap().get("email");
                            createBundleAndFinish(name, "facebook", "aToken", "rToken");
                        }
                    }
                }
        );
        Request.executeBatchAsync(request);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    @OnClick(R.id.sign_in_button)
    void attemptLogin() {
        if (!checkNet()) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mPasswordConfirmView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        String pass = mPasswordView.getText().toString();
        final String passConfirm = mPasswordConfirmView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isValidEmail(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid password.
        if (TextUtils.isEmpty(pass) || !isPasswordValid(pass)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (mIsRegister && !pass.equals(passConfirm)) {
            mPasswordConfirmView.setError(getString(R.string.error_incorrect_password));
            focusView = mPasswordConfirmView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            initService(email, pass);
        }
    }

//    todo implement mail/pass login
    private void initService(String email, String passPlain) {
        final String pass = Utils.SHA1(passPlain);
        if (mIsRegister) {
        } else {
        }
        createBundleAndFinish(email, pass, "aToken", "rToken");
    }

    private boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private boolean supportsGooglePlayServices() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) ==
                ConnectionResult.SUCCESS;
    }

    private void createBundleAndFinish(String name, String pass, String aToken, String rToken) {
        AuthUtils.createAccount(this, name, pass, aToken, rToken);
        PrefUtils.setUserLogin(this, name);

        final Bundle bundle = new Bundle(3);
        bundle.putString(AccountManager.KEY_ACCOUNT_NAME, name);
        bundle.putString(AccountManager.KEY_ACCOUNT_TYPE, AuthUtils.ACCOUNT_TYPE);
        bundle.putString(AccountManager.KEY_AUTHTOKEN, aToken);
        finishSetup(RESULT_CODE_SUCCESS, bundle);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        final InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginStatusView.setVisibility(View.VISIBLE);
        mLoginStatusView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });

        mLoginFormView.setVisibility(View.VISIBLE);
        mLoginFormView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);

                        if (show) {
                            imm.hideSoftInputFromWindow(mEmailView.getWindowToken(), 0);
                        }
                    }
                });

        if (!show) {
            mEmailView.requestFocus();
            imm.showSoftInput(mEmailView, InputMethodManager.SHOW_IMPLICIT);

        }
    }

    public void finishSetup(int resultCode, Bundle bundle) {
        switch (resultCode) {
            case RESULT_CODE_SUCCESS: {
                setAccountAuthenticatorResult(bundle);
                final Intent i = new Intent();
                i.putExtras(bundle);
                setResult(Activity.RESULT_OK, i);
                if (mFinishIntent != null) {
                    mFinishIntent.addCategory(POST_AUTH_CATEGORY);
                    startActivity(mFinishIntent);
                }
                break;
            }
            case RESULT_CODE_CANCELED: {
                setResult(Activity.RESULT_CANCELED);
//                AuthUtils.signOut(this);
                break;
            }
            case RESULT_CODE_FAILURE: {
//                AuthUtils.signOut(this);
                break;
            }
        }

        finish();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode,
                                    final Intent data) {
        if (requestCode == ACCOUNT_REQUEST && resultCode == RESULT_OK) {
            mAccountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            new GetTokenTask().execute();
        } else if (requestCode == ACCOUNT_REQUEST) {
            showProgress(false);
        }

        if (requestCode == RESOLVE_GOOGLE_REQUEST && resultCode == RESULT_OK) {
            new GetTokenTask().execute();
        }

        if (requestCode == Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE) {
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        attemptLogin();
        return true;
    }

    class GetTokenTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            return getToken();
        }

        @Override
        protected void onPostExecute(String token) {
            if (token != null) {
                createBundleAndFinish(mAccountName, "google", "aToken", "rToken");
            }
        }

        private String getToken() {
            String accessToken;
            try {
                accessToken = GoogleAuthUtil.getToken(getApplicationContext(),
                        mAccountName,
                        "oauth2:https://www.googleapis.com/auth/userinfo.email"
                                + " https://www.googleapis.com/auth/userinfo.profile"
                );
            } catch (IOException transientEx) {
                // network or server error, the call is expected to succeed if you try again later.
                // Don't attempt to call again immediately - the request is likely to
                // fail, you'll hit quotas or back-off.
                accessToken = null;
            } catch (UserRecoverableAuthException e) {
                // Recover
                startActivityForResult(e.getIntent(), RESOLVE_GOOGLE_REQUEST);
                accessToken = null;
            } catch (GoogleAuthException authEx) {
                // Failure. The call is not expected to ever succeed so it should not be
                // retried.
                accessToken = null;
            } catch (Exception e) {
//            throw new RuntimeException(e);
                accessToken = null;
            }

            return accessToken;
        }
    }
}
