package sk.app.ui;

import android.app.Activity;
import android.os.Bundle;

import sk.app.Utils.LogUtils;
import sk.app.account.AuthUtils;

public abstract class BaseActivity extends Activity {
    private static final String TAG = LogUtils.makeLogTag(BaseActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!AuthUtils.isUserAuthenticated(this)) {
            LogUtils.LOGD(TAG, "exiting:"
                    + " isAuthenticated=" + AuthUtils.isUserAuthenticated(this));
            AuthUtils.startAuthenticationFlow(this, getIntent());
            finish();
        }
    }
}