package sk.app.ui.fragment;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

public class AlertDialogFragment extends DialogFragment {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private String mTitle;
    private String mMessage;

    public static DialogFragment newInstance(String title, String message) {
        final DialogFragment fragment = new AlertDialogFragment();
        final Bundle args = new Bundle(2);
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        final Fragment prev = manager.findFragmentByTag(tag);
        if (prev == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString(TITLE);
            mMessage = arguments.getString(MESSAGE);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Builder(getActivity())
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setNeutralButton(android.R.string.ok, null)
                .setCancelable(false)
                .create();
    }

}
